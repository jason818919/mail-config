# Synopsis
Configure dovecot imaps server automatically.

# Code Example
	
# Motivation
To simplify the ordinary routines on setting the imaps server.

# Installation
Download the source code and run the installation script as follows.
```bash

```

# API Reference

# Tests
Run the application on terminal.
```bash
./install
```

# Contributor
Jason Chen
